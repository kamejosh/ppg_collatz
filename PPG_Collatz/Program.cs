﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using ppg_collatz;
using System.Collections.Concurrent;

namespace PPG_Collatz
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 form1 = new Form1();

            Thread t = new Thread(() => runCollatz(form1));
            t.Start();

            Application.Run(form1);

            //runCollatz();
        }

        public static void runCollatz(Form1 f)
        {
            int max = 100000;
            int step = 10000;
            bool bDone = false;
            var t1 = Task.Factory.StartNew(() => CalculateRows(1, max, ref bDone, f)); //could be optimized if this thread runs until Picture is finished
            var t2 = Task.Factory.StartNew(() => DrawPicture(max, step, ref bDone, f)); // final picture after last calculationf));
            Task.WaitAll(t1, t2);

            f.Invoke((MethodInvoker)delegate
            {
                f.pictureBox1.DrawToBitmap(f.resultBitmap, new System.Drawing.Rectangle(0, 0, f.Width, f.Height));
                f.resultBitmap.Save("collatz.bmp");
            });
        }

        public static void CalculateRows(int start, int end, ref bool bDone, Form1 f)
        {
            var result = new ConcurrentStack<List<bool>>();
            Parallel.For(start, end + 1, i =>
            {
                f.results.Push(Collatz.Calculate(i));
            });
            bDone = true;
        }

        public static void DrawPicture(int max, int step, ref bool bDone, Form1 f)
        {
            f.m_step = step;
            while(f.results.Count > 0 || !bDone)
            {
                f.Invoke((MethodInvoker)delegate
                {
                    f.pictureBox1.Refresh();
                    f.pictureBox1.DrawToBitmap(f.resultBitmap, new System.Drawing.Rectangle(0, 0, f.pictureBox1.Width, f.pictureBox1.Height));
                });
            }
        }
    }
}