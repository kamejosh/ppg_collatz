﻿using System.Collections.Generic;
using System.Diagnostics;

namespace PPG_Collatz
{
    public class Collatz
    {
        public static List<bool> Calculate(long number)
        {
            var result = new List<bool>();

            while (number != 1)
            {
                if (number % 2 == 1)
                {
                    result.Insert(0, false);
                    number = (3 * number + 1);
                }
                
                while (number % 2 == 0)
                {
                    result.Insert(0, true);
                    number /= 2;
                }
                
            }
            return result;
        }
    }
}