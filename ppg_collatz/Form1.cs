﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ppg_collatz
{
    public partial class Form1 : Form
    {
        public ConcurrentStack<List<bool>> results = new ConcurrentStack<List<bool>>();
        public Bitmap resultBitmap = new Bitmap(3840, 2160);
        public int m_step;

        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            // Declares the Graphics object and sets it to the Graphics object  
            // supplied in the PaintEventArgs.
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            lock (resultBitmap)
            {
                e.Graphics.DrawImage(resultBitmap, 0, 0);
            }
            int i = 0;
            for (i = 0; i < m_step;)
            {
                List<bool> list;
                results.TryPop(out list);

                if (list != null)
                {
                    int alpha = 5;
                    Color c = Color.FromArgb(alpha, 0, 128, 128);
                    double h;

                    var angle = -45;
                    int posX = 0;
                    int posY = (int)(pictureBox1.Height / 1.3);
                    foreach (var line in list)
                    {
                        if (line)
                        {
                            angle += 5;
                            h = (int)((((float)angle + 45) / 75) * 255);
                            drawLine(line, ref posX, ref posY, angle, ref e, ColorFromHSV(h, 1, 0.5, alpha));
                        }
                        else
                        {
                            angle -= 9;
                            h = (int)((((float)angle + 45) / 75) * 255);
                            drawLine(line, ref posX, ref posY, angle, ref e, ColorFromHSV(h, 1, 0.5, alpha));
                        }
                    }
                    i++;
                }
                else if(results.IsEmpty)
                {
                    break;
                }
            }
            int n;
            Int32.TryParse(counterLabel.Text, out n);
            counterLabel.Text = (n+i).ToString();
            label1.Refresh();
            counterLabel.Refresh();
        }

        public static void ColorToHSV(Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }

        public static Color ColorFromHSV(double hue, double saturation, double value, int alpha)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(alpha, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(alpha, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(alpha, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(alpha, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(alpha, t, p, v);
            else
                return Color.FromArgb(alpha, v, p, q);
        }

        private void drawLine(bool direction, ref int xFrom, ref int yFrom, double angle, ref PaintEventArgs e, Color c)
        {
            var len = 10;
            Pen p = new Pen(c, 2);
            var newAngle = (angle % 360) * Math.PI / 180;
            var yDiff = (int)(Math.Sin(newAngle) * len);
            var xDiff = (int)(Math.Cos(newAngle) * len);
            int xTo = xFrom + xDiff;
            int yTo = yFrom + yDiff;

            e.Graphics.DrawLine(p, (int)xFrom, (int)yFrom, (int)(xTo), (int)(yTo));

            xFrom = xTo;
            yFrom = yTo;
            p.Dispose();
        }
    }
}
